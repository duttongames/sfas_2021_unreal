// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeGenerator.h"

// Sets default values
AMazeGenerator::AMazeGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMazeGenerator::BeginPlay()
{
	Super::BeginPlay();

	float xSpawn = -16200.0f;
	float ySpawn = -16200.0f;

	int chunkCounter = 0;

	//Spawn in a randomly generated maze.
	for (int i = 0; i < 30; i++)
	{
		FActorSpawnParameters SpawnInfo;
		FVector worldPosition(xSpawn, ySpawn, 0);
		FTransform worldXForm(GetActorRotation(), worldPosition, GetActorScale());
		AUnrealSFASMaze* ActorRef = GetWorld()->SpawnActor<AUnrealSFASMaze>(mazeChunk, worldXForm, SpawnInfo);

		xSpawn += 7550.0f;
		chunkCounter++;

		if (chunkCounter == 5)
		{
			xSpawn = -16200.0f;
			ySpawn += 8000.0f;
			chunkCounter = 0;
		}
	}
}

