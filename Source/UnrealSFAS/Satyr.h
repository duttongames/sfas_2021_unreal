// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UnrealSFASCharacter.h"
#include "EnemyStats.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "Satyr.generated.h"

UCLASS()
class UNREALSFAS_API ASatyr : public ACharacter
{
	GENERATED_BODY()

public:
	EnemyStats* stats = new EnemyStats(this, 40, 5, 5, 3);
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float detection;

	UPROPERTY(EditDefaultsOnly, Category = "Pathfinding")
	TSoftObjectPtr<AActor> player;
	TSoftObjectPtr<AActor> currentNode;

	float nodeTimer = 0;
	float timeSinceLastDetected = 0;

	UFUNCTION()
	void OnPawnSeen(APawn* seenPawn);
	UFUNCTION()
	void OnHearNoise(APawn* heardPawn, const FVector& Location, float Volume);
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	// Sets default values for this character's properties
	ASatyr();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
