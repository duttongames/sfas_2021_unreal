// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Graph.h"
#include "Satyr.h"
#include "Minotaur.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"
#include "UnrealSFASMaze.generated.h"

static bool minotaurSpawned = false;
static int satyrAmount = 0;

UCLASS()
class UNREALSFAS_API AUnrealSFASMaze : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASMaze();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditDefaultsOnly, Category = Maze)
	UStaticMesh* WallMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ActorSpawning")
	TSubclassOf<APathNode> Node;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ActorSpawning")
	TSubclassOf<AMinotaur> minotaurEnemy;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ActorSpawning")
	TSubclassOf<ASatyr> satyrEnemy;

	Graph* mazeGraph = new Graph();
	vector<vector<APathNode*>> nodesArray;
	vector<APathNode*> validNodes;
};
