// Fill out your copyright notice in the Description page of Project Settings.

#include "Minotaur.h"

// Sets default values
AMinotaur::AMinotaur()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMinotaur::BeginPlay()
{
	Super::BeginPlay();
	stats->player = player.Get();
	FindComponentByClass<UPawnSensingComponent>()->OnSeePawn.AddDynamic(this, &AMinotaur::OnPawnSeen);
	FindComponentByClass<UPawnSensingComponent>()->OnHearNoise.AddDynamic(this, &AMinotaur::OnHearNoise);
	FindComponentByClass<UBoxComponent>()->OnComponentBeginOverlap.AddDynamic(this, &AMinotaur::BeginOverlap);
}

void AMinotaur::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (stats != nullptr)
	{
		delete stats;
	}

	Super::EndPlay(EndPlayReason);
}

// Called every frame
void AMinotaur::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (detection >= 100)
	{
		stats->aiMode = 1;
	}

	if (timeSinceLastDetected > 4 && detection > 0)
	{
		detection -= 10 * DeltaTime;

		if (detection < 0)
		{
			detection = 0;
		}
	}

	stats->Move();

	stats->CheckHealth();
	stats->DamagePlayer();

	stats->damageTimer -= DeltaTime;
	timeSinceLastDetected += DeltaTime;
}

// Called to bind functionality to input
void AMinotaur::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMinotaur::OnPawnSeen(APawn* seenPawn)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Current distance is %f"), (GetActorLocation() - seenPawn->GetActorLocation()).Size()));
	detection += 10;
	timeSinceLastDetected = 0;
}

void AMinotaur::OnHearNoise(APawn* heardNoise, const FVector& Location, float Volume)
{
	detection += 5;
	timeSinceLastDetected = 0;
}

void AMinotaur::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag(TEXT("Node")) && Cast<APathNode>(OtherActor) != stats->previousNode)
	{
		stats->ChangeTarget(OtherActor);
	}
}