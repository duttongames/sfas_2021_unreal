// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <iostream>
#include <map>
#include "PathNode.h"
#include "CoreMinimal.h"

using namespace std;

struct Vertex
{
	APathNode* node;
	vector<APathNode*> edges;
};

class UNREALSFAS_API Graph
{
public:
	Graph();
	~Graph();

	map<APathNode*, Vertex*> nodes;
};
