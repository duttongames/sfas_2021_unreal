// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyStats.h"

EnemyStats::EnemyStats(AActor* actor, float hp, float dmg, float spd, int intel)
{
	enemy = actor;

	maxHealth = hp;
	currentHealth = hp;
	damage = dmg;
	speed = spd;
	intelligence = intel;
}

EnemyStats::~EnemyStats()
{

}