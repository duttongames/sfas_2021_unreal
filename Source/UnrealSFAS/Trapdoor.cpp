// Fill out your copyright notice in the Description page of Project Settings.


#include "Trapdoor.h"

// Sets default values
ATrapdoor::ATrapdoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATrapdoor::BeginPlay()
{
	Super::BeginPlay();
	FindComponentByClass<UBoxComponent>()->OnComponentBeginOverlap.AddDynamic(this, &ATrapdoor::BeginOverlap);
}

void ATrapdoor::Tick(float DeltaTime)
{
	if (open)
	{
		FRotator nextRotation(GetActorRotation().Pitch + 80 * DeltaTime, 0, 0);
		SetActorRotation(nextRotation);
	}
}

void ATrapdoor::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(AUnrealSFASCharacter::StaticClass()))
	{
		open = true;
	}
}