// Fill out your copyright notice in the Description page of Project Settings.

#include "Satyr.h"
#include "PathNode.h"

// Sets default values
ASatyr::ASatyr()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASatyr::BeginPlay()
{
	Super::BeginPlay();
	stats->player = player.Get();
	FindComponentByClass<UPawnSensingComponent>()->OnSeePawn.AddDynamic(this, &ASatyr::OnPawnSeen);
	FindComponentByClass<UPawnSensingComponent>()->OnHearNoise.AddDynamic(this, &ASatyr::OnHearNoise);
	FindComponentByClass<UBoxComponent>()->OnComponentBeginOverlap.AddDynamic(this, &ASatyr::BeginOverlap);
}

void ASatyr::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (stats != nullptr)
	{
		delete stats;
	}

	Super::EndPlay(EndPlayReason);
}

// Called every frame
void ASatyr::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (detection >= 100)
	{
		stats->aiMode = 1;
	}

	if (timeSinceLastDetected > 4 && detection > 0)
	{
		detection -= 10 * DeltaTime;

		if (detection < 0)
		{
			detection = 0;
		}
	}

	stats->Move();

	stats->CheckHealth();
	stats->DamagePlayer();

	stats->damageTimer -= DeltaTime;
	timeSinceLastDetected += DeltaTime;
}

// Called to bind functionality to input
void ASatyr::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ASatyr::OnPawnSeen(APawn* seenPawn)
{
	detection += 10;
	timeSinceLastDetected = 0;
}

void ASatyr::OnHearNoise(APawn* heardNoise, const FVector& Location, float Volume)
{
	detection += 10;
	timeSinceLastDetected = 0;
}

void ASatyr::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag(TEXT("Node")) && Cast<APathNode>(OtherActor) != stats->previousNode)
	{
		stats->ChangeTarget(OtherActor);
	}
}