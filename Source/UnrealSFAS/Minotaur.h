// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UnrealSFASCharacter.h"
#include "EnemyStats.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Minotaur.generated.h"

UCLASS()
class UNREALSFAS_API AMinotaur : public ACharacter
{
	GENERATED_BODY()

public:
	EnemyStats* stats = new EnemyStats(this, 100000, 50, 8, 5);
	UPROPERTY(EditDefaultsOnly, Category = "Pathfinding")
	TSoftObjectPtr<AActor> player;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float detection;

	float nodeTimer = 0;
	float timeSinceLastDetected = 0;

	UFUNCTION()
	void OnPawnSeen(APawn* seenPawn);
	UFUNCTION()
	void OnHearNoise(APawn* heardPawn, const FVector& Location, float Volume);
	// Sets default values for this character's properties
	AMinotaur();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};