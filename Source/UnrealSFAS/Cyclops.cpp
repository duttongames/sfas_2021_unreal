// Fill out your copyright notice in the Description page of Project Settings.


#include "Cyclops.h"

// Sets default values
ACyclops::ACyclops()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACyclops::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACyclops::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (detection >= 100)
	{
		stats->aiMode = 1;
	}

	if (timeSinceLastDetected > 4 && detection > 0)
	{
		detection -= 10 * DeltaTime;

		if (detection < 0)
		{
			detection = 0;
		}
	}

	if (stats->aiMode == 1)
	{
		stats->Move();
	}

	stats->CheckHealth();
	stats->DamagePlayer();

	stats->damageTimer -= DeltaTime;
}

// Called to bind functionality to input
void ACyclops::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

