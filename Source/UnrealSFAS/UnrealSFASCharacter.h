// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once
#include <vector>
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/BoxComponent.h"
#include "Arrow.h"
#include "PathNode.h"
#include "UnrealSFASCharacter.generated.h"

using namespace std;

UCLASS(config=Game)
class AUnrealSFASCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	vector<APathNode*> latestNodes;

	//Rock object.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Tools")
	TSubclassOf<AActor> rock;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float maxHealth = 100.0f;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float currentHealth = 100.0f;
	int currentItem = 0; //0 = None 1 = Winged Sandals 2 = Cap of Invisibility
	bool sprinting = false;

	AUnrealSFASCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/*Causes the player to crouch.*/
	void Crouch()
	{
		
	};

	/*Stops crouching.*/
	void StopCrouching()
	{
		
	};

	/** Starts sprinting*/
	void Sprint()
	{
		GetCharacterMovement()->MaxWalkSpeed = 1000;
		sprinting = true;
	};

	/** Stops sprinting*/
	void StopSprinting()
	{
		GetCharacterMovement()->MaxWalkSpeed = 600;
		sprinting = false;
	};

	/*Throws a rock to distract enemies.*/
	void ThrowRock()
	{
		FActorSpawnParameters SpawnInfo;
		FTransform worldXForm(GetActorRotation(), GetActorLocation() + GetActorForwardVector() * 100.0f, GetActorScale());
		AActor* newRock = GetWorld()->SpawnActor<AActor>(rock, worldXForm, SpawnInfo);
	}

	/*Uses the currently selected item.*/
	void UseItem()
	{
		switch (currentItem)
		{
			//Winged Sandals
			case 1:
				GetCharacterMovement()->MaxWalkSpeed = 2000;
				GetCharacterMovement()->GroundFriction = 2;
				GetCharacterMovement()->BrakingDecelerationWalking = 1;
				break;

			//Cap of Invisibility
			case 2:
				GetMesh()->ToggleVisibility();
				break;
		}
	}

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

