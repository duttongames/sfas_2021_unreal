// Fill out your copyright notice in the Description page of Project Settings.


#include "Powerup.h"
#include "UnrealSFASCharacter.h"

// Sets default values
APowerup::APowerup()
{
 	// Set this actor to not call Tick() every frame.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void APowerup::BeginPlay()
{
	Super::BeginPlay();
	FindComponentByClass<UBoxComponent>()->OnComponentBeginOverlap.AddDynamic(this, &APowerup::BeginOverlap);
}

void APowerup::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(AUnrealSFASCharacter::StaticClass()))
	{
		Cast<AUnrealSFASCharacter>(OtherActor)->currentItem = powerupID;
		Destroy();
	}
}