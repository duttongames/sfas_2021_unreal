// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UnrealSFASCharacter.h"
#include "Graph.h"
#include "PathNode.h"
#include "CoreMinimal.h"
#include "Kismet/KismetMathLibrary.h"
#include "Misc/App.h"

/**
 * 
 */
class UNREALSFAS_API EnemyStats
{
public:
	AActor* player; //The player character.
	AActor* enemy; //The enemy actor this stats script is attached to.
	APathNode* currentNode;
	APathNode* previousNode;
	Graph* currentMazeGraph;

	float maxHealth; //The starting health of the enemy.
	float currentHealth; //The current health of the enemy.
	float damage; //The damage the enemy deals to the player.
	float speed; //The speed at which the enemy moves.
	int intelligence; //The intelligence of the enemy, affects AI decisions.
	int aiMode = 0; //0 = Wander 1 = Pursuit/Chase 2 = Search

	float damageRate = 2;
	float damageTimer;

	EnemyStats(AActor* actor, float hp, float dmg, float spd, int intel);
	~EnemyStats();

	//Checks the health of the enemy and destroys them if it is less than or equal to 0.
	void CheckHealth()
	{
		if (currentHealth <= 0)
		{
			enemy->Destroy();
		}
	}

	//Damages the player when they are within range.
	void DamagePlayer()
	{
		if ((player->GetActorLocation() - enemy->GetActorLocation()).Size() < 200 && damageTimer <= 0)
		{
			AUnrealSFASCharacter* playerCharacter = Cast<AUnrealSFASCharacter>(player);
			playerCharacter->currentHealth -= damage;

			if (playerCharacter->currentHealth <= 0)
			{
				aiMode = 0;
				playerCharacter->Destroy();
			}

			damageTimer = damageRate;
		}
	}

	//Moves the actor forward based on their speed.
	void Move()
	{
		float aiModifier = 1.0f;

		if (aiMode == 0)
		{
			if (currentNode != nullptr)
			{
				FVector nextPosition(enemy->GetActorLocation().X + 0.5f, enemy->GetActorLocation().Y, enemy->GetActorLocation().Z);
				FTransform worldXForm(enemy->GetActorRotation(), nextPosition, enemy->GetActorScale());
				enemy->SetActorLocation(enemy->GetActorLocation() += enemy->GetActorForwardVector() * 100.0f * FApp::GetDeltaTime());
			}
		}

		if (aiMode == 1)
		{
			//Face the player or their latest Node while chasing them.
			AUnrealSFASCharacter* playerScript = Cast<AUnrealSFASCharacter>(player);

			if (playerScript->latestNodes.size() > 0)
			{
				enemy->SetActorRotation(UKismetMathLibrary::FindLookAtRotation(enemy->GetActorLocation(), playerScript->latestNodes[playerScript->latestNodes.size() - 1]->GetActorLocation()));
			}

			else
			{
				enemy->SetActorRotation(UKismetMathLibrary::FindLookAtRotation(enemy->GetActorLocation(), player->GetActorLocation()));
			}

			//Move faster when chasing the player.
			aiModifier = 2.0f;
		}

		FVector nextPosition(enemy->GetActorLocation().X + 0.5f, enemy->GetActorLocation().Y, enemy->GetActorLocation().Z);
		FTransform worldXForm(enemy->GetActorRotation(), nextPosition, enemy->GetActorScale());
		enemy->SetActorLocation(enemy->GetActorLocation() += enemy->GetActorForwardVector() * (speed * (50 * aiModifier)) * FApp::GetDeltaTime());
	}

	//Changes the target Node on collision.
	void ChangeTarget(AActor* OtherActor)
	{
		APathNode* node = Cast<APathNode>(OtherActor);

		if (currentMazeGraph->nodes[node] != nullptr && currentMazeGraph->nodes[node]->edges.size() > 0)
		{
			int nextNode = FMath::RandRange(0, currentMazeGraph->nodes[node]->edges.size() - 1);

			if (previousNode != nullptr && currentMazeGraph->nodes[node]->edges.size() > 1 && currentMazeGraph->nodes[node]->edges[nextNode] == previousNode)
			{
				if (nextNode > 0)
				{
					nextNode--;
				}

				else if (nextNode < currentMazeGraph->nodes[node]->edges.size())
				{
					nextNode++;
				}
			}

			if (currentMazeGraph->nodes[node]->edges.size() > 0 && currentMazeGraph->nodes[node]->edges[nextNode] != nullptr)
			{
				currentNode = currentMazeGraph->nodes[node]->edges[nextNode];
				enemy->SetActorRotation(UKismetMathLibrary::FindLookAtRotation(enemy->GetActorLocation(), currentNode->GetActorLocation()));
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, (TEXT("Change Direction")));
			}

			AUnrealSFASCharacter* playerScript = Cast<AUnrealSFASCharacter>(player);

			if (playerScript->latestNodes.size() > 0)
			{
				if (playerScript->latestNodes[0] == node)
				{
					playerScript->latestNodes.erase(playerScript->latestNodes.begin());
				}
			}

			previousNode = node;
		}
	}
};
