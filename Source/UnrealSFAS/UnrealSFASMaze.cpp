// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASMaze.h"
#include <vector>
#include <string>
#include <bitset>

using namespace std;

//Checks if a space in the maze has empty neighbours.
void CheckSpace(vector<string>& maze, vector<vector<int>>& visitedList, int rowNumber, int columnNumber, string& rowString)
{
	//Determine if there is only one empty neighbour.
	int emptySpaces = 0;

	if (columnNumber - 1 > 0)
	{
		if (rowString[columnNumber - 1] == '0')
		{
			emptySpaces++;
		}
	}

	if (columnNumber + 1 < rowString.length() - 1)
	{
		if (rowString[columnNumber + 1] == '0')
		{
			emptySpaces++;
		}
	}

	if (rowNumber + 1 < maze.size() - 1)
	{
		if (maze[rowNumber + 1][columnNumber] == '0')
		{
			emptySpaces++;
		}
	}

	if (rowNumber - 1 > 0)
	{
		if (maze[rowNumber - 1][columnNumber] == '0')
		{
			emptySpaces++;
		}
	}

	if (emptySpaces == 1)
	{
		//Mark the space as visited.
		vector<int> space = { rowNumber, columnNumber };
		visitedList.push_back(space);

		//Make the space empty.
		rowString[columnNumber] = '0';

		maze[rowNumber] = rowString;
	}
}

//Generates a new maze chunk using a recursive backtracking method.
void RecursiveBacktracking(vector<string> &maze)
{
	vector<vector<int>> visitedList;

	int rowNumber = FMath::RandRange(5, 14);
	int columnNumber = FMath::RandRange(5, 14);
	string direction = "EAST";
	string directions[] = { "NORTH", "WEST", "SOUTH", "EAST" };
	vector<int> firstSpace = { rowNumber, columnNumber };

	direction = directions[FMath::RandRange(0, 3)];

	maze[rowNumber][columnNumber] = '0';
	visitedList.push_back(firstSpace);

	int loopCount = 0;

	while (visitedList.size() > 0)
	{
		string currentRow = maze[rowNumber];
		CheckSpace(maze, visitedList, rowNumber, columnNumber, currentRow);

		if (direction == "NORTH" && rowNumber > 0)
		{
			rowNumber--;
		}

		else if (direction == "EAST" && columnNumber < currentRow.size() - 1)
		{
			columnNumber++;
		}

		else if (direction == "SOUTH" && rowNumber < maze.size() - 1)
		{
			rowNumber++;
		}

		else if (direction == "WEST" && columnNumber > 0)
		{
			columnNumber--;
		}

		else
		{
			visitedList.pop_back();

			if (visitedList.size() > 0)
			{
				vector<string> possibleDirections;

				if (rowNumber > 0)
				{
					possibleDirections.push_back("NORTH");
				}

				if (rowNumber < maze.size() - 1)
				{
					possibleDirections.push_back("SOUTH");
				}

				if (columnNumber > 0)
				{
					possibleDirections.push_back("WEST");
				}

				if (columnNumber < currentRow.size() - 1)
				{
					possibleDirections.push_back("EAST");
				}

				if (possibleDirections.size() > 0)
				{
					rowNumber = visitedList[visitedList.size() - 1][0];
					columnNumber = visitedList[visitedList.size() - 1][1];
					direction = possibleDirections[FMath::RandRange(0, possibleDirections.size() - 1)];
				}
			}
		}

		loopCount++;

		if (loopCount > 1000)
		{
			visitedList.clear();
		}
	}
}

vector<uint32> GenerateMazeChunk(int chunkSize)
{
	vector<string> chunkString;
	vector<uint32> mazeChunk;

	//Make every space(except the first) a wall to start with.
	for (int i = 0; i < chunkSize; i++)
	{
		chunkString.push_back("1111111111111111111");
	}

	RecursiveBacktracking(chunkString);

	for (int i = 0; i < chunkString.size(); i++)
	{
		uint32 test = bitset<32>(chunkString[i]).to_ulong();
		mazeChunk.push_back(test);
	}

	return mazeChunk;
}

// Sets default values
AUnrealSFASMaze::AUnrealSFASMaze()
{
 	// No need to tick the maze at this point
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AUnrealSFASMaze::BeginPlay()
{
	Super::BeginPlay();

	if (WallMesh)
	{
		const int32 mazeSize = 20;
		const float blockSize = 400.0f;
		const float blockWidth = 4.0f;
		const float blockHeight = 5.0f;
		const float blockZPos = 100.0f;

		// Array of binary values: 1 = wall, 0 = space
		uint32 mazeArrayOld[mazeSize] = {	0b11111111111111111111,
										0b10100000000000000001,
										0b10101010111111111101,
										0b10101010001111000101,
										0b10101010100001010001,
										0b10101010111111111111,
										0b10101010000000000001,
										0b10001011111111111101,
										0b11111000000000000101,
										0b10001111111111111101,
										0b10101111111111111001,
										0b10100000000000001101,
										0b10111011111111101001,
										0b10100010000000101011,
										0b10101110111110101001,
										0b10100000100010101101,
										0b10101110001110100001,
										0b10111111111110111101,
										0b10000000000000000001,
										0b11111111111111111101 };

		vector<uint32> mazeArray = GenerateMazeChunk(20);

		float xPos = 0.0f;
		float yPos = 0.0f;
		FQuat worldRotation(FVector(0.0f, 0.0f, 1.0f), 0.0f);
		FVector worldScale(blockWidth, blockWidth, blockHeight);
		uint32 mazeRow;

		float xOffset = GetActorLocation().X;
		float yOffset = GetActorLocation().Y;

		USceneComponent* rootComponent = GetRootComponent();

		// Loop through the binary values to generate the maze as static mesh components attached to the root of this actor
		for (int32 i = 0; i < mazeSize; i++)
		{
			yPos = static_cast<float>(i - (mazeSize / 2)) * blockSize;
			mazeRow = mazeArray[i];
			string rowString = bitset<32>(mazeArray[i]).to_string();

			for (int32 j = 0; j < mazeSize; j++)
			{
				xPos = static_cast<float>(j - (mazeSize / 2)) * blockSize;

				uint32 mazeValue = (mazeRow >> (mazeSize - (j))) & 1;

				if (mazeValue)
				{
					UStaticMeshComponent* meshComponent = NewObject<UStaticMeshComponent>(this);
					FVector worldPosition(xPos + xOffset, yPos + yOffset, blockZPos);
					FTransform worldXForm(worldRotation, worldPosition, worldScale);

					meshComponent->SetStaticMesh(WallMesh);
					meshComponent->SetWorldTransform(worldXForm);
					meshComponent->AttachToComponent(rootComponent, FAttachmentTransformRules::KeepWorldTransform);
					meshComponent->RegisterComponent();

					if (j == 0)
					{
						vector<APathNode*> empty;
						nodesArray.push_back(empty);
					}

					nodesArray[i].push_back(nullptr);
				}

				else
				{
					FVector worldPosition(xPos + xOffset, yPos + yOffset, blockZPos);
					FTransform worldXForm(worldRotation, worldPosition, worldScale / 4);
					FActorSpawnParameters SpawnInfo;
					APathNode* nodeRef = GetWorld()->SpawnActor<APathNode>(Node, worldXForm, SpawnInfo);
					validNodes.push_back(nodeRef);

					if (j == 0)
					{
						vector<APathNode*> empty;
						nodesArray.push_back(empty);
					}

					nodesArray[i].push_back(nodeRef);

					//Add the Node to the graph.
					Vertex* nodeVertex = new Vertex();
					nodeVertex->node = nodeRef;
					mazeGraph->nodes[nodeRef] = nodeVertex;
				}
			}
		}
	}

	for (int i = 0; i < nodesArray.size(); i++)
	{
		for (int j = 0; j < nodesArray[i].size(); j++)
		{
			if (nodesArray[i][j] != nullptr)
			{
				if (i > 0 && nodesArray[i - 1][j] != nullptr)
				{
					mazeGraph->nodes[nodesArray[i][j]]->edges.push_back(nodesArray[i - 1][j]);
				}

				if (i < 19 && nodesArray[i + 1][j] != nullptr)
				{
					mazeGraph->nodes[nodesArray[i][j]]->edges.push_back(nodesArray[i + 1][j]);
				}

				if (j > 0 && nodesArray[i][j - 1] != nullptr)
				{
					mazeGraph->nodes[nodesArray[i][j]]->edges.push_back(nodesArray[i][j - 1]);
				}

				if (j < 19 && nodesArray[i][j + 1] != nullptr)
				{
					mazeGraph->nodes[nodesArray[i][j]]->edges.push_back(nodesArray[i][j + 1]);
				}
			}
		}
	}

	int32 nodeToSpawn = FMath::RandRange(0, validNodes.size() - 1);
	FTransform worldXForm(GetActorRotation(), validNodes[nodeToSpawn]->GetActorLocation(), GetActorScale());
	FActorSpawnParameters SpawnInfo;

	while (mazeGraph->nodes[validNodes[nodeToSpawn]]->edges.size() < 1)
	{
		nodeToSpawn = FMath::RandRange(0, validNodes.size() - 1);
	}

	if (minotaurSpawned == true || FMath::RandRange(0, 15) == 0)
	{
		if (satyrAmount < 3)
		{
			//ASatyr* satyrRef = GetWorld()->SpawnActor<ASatyr>(satyrEnemy, worldXForm, SpawnInfo);
			//satyrRef->stats->currentNode = mazeGraph->nodes[validNodes[nodeToSpawn]]->edges[FMath::RandRange(0, mazeGraph->nodes[validNodes[nodeToSpawn]]->edges.size() - 1)];
			//satyrRef->stats->currentMazeGraph = mazeGraph;
			satyrAmount++;
		}
	}

	else
	{
		AMinotaur* minotaurRef = GetWorld()->SpawnActor<AMinotaur>(minotaurEnemy, worldXForm, SpawnInfo);
		minotaurRef->stats->currentNode = mazeGraph->nodes[validNodes[nodeToSpawn]]->edges[FMath::RandRange(0, mazeGraph->nodes[validNodes[nodeToSpawn]]->edges.size() - 1)];
		minotaurRef->stats->currentMazeGraph = mazeGraph;
		minotaurSpawned = true;
	}
}