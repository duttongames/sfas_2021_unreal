// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UnrealSFASCharacter.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Trapdoor.generated.h"

UCLASS()
class UNREALSFAS_API ATrapdoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrapdoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool open = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
