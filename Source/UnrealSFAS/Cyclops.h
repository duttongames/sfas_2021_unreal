// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyStats.h"
#include "Cyclops.generated.h"

UCLASS()
class UNREALSFAS_API ACyclops : public ACharacter
{
	GENERATED_BODY()

public:
	EnemyStats* stats = new EnemyStats(this, 250, 45, 3, 1);
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float detection;

	float nodeTimer = 0;
	float timeSinceLastDetected = 0;
	// Sets default values for this character's properties
	ACyclops();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
