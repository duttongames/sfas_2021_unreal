// Fill out your copyright notice in the Description page of Project Settings.


#include "ArrowTrap.h"

// Sets default values
AArrowTrap::AArrowTrap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AArrowTrap::BeginPlay()
{
	Super::BeginPlay();
	FindComponentByClass<UBoxComponent>()->OnComponentBeginOverlap.AddDynamic(this, &AArrowTrap::BeginOverlap);
}

// Called every frame
void AArrowTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArrowTrap::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(AUnrealSFASCharacter::StaticClass()))
	{
		FActorSpawnParameters SpawnInfo;
		FVector arrowLocation = FVector(GetActorLocation().X - 400, GetActorLocation().Y, GetActorLocation().Z + 150);
		FTransform worldXForm(GetActorRotation(), arrowLocation, FVector(0.1f, 0.8f, 0.1f));
		AArrow* ActorRef = GetWorld()->SpawnActor<AArrow>(arrow, worldXForm, SpawnInfo);
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Test")));
	}
}